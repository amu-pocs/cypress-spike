describe('Finding Elements', () => {

  it('should only show small screen content', () => {
    cy.visit('http://localhost:4200/home')
    cy.viewport(320, 660);
    cy.get('.extra-small-screen').should('be.visible');
    cy.get('.small-to-large-screen').should('be.visible');
    cy.get('.large-screen').should('not.be.visible');
  });

  it('should only show small and mid screen content', () => {
    cy.visit('http://localhost:4200/home')
    cy.viewport(1080, 660);
    cy.get('.extra-small-screen').should('not.be.visible');
    cy.get('.small-to-large-screen').should('be.visible');
    cy.get('.large-screen').should('be.visible');
  });

  it('should only show small screen content', () => {
    cy.visit('http://localhost:4200/home');
    cy.viewport(1690, 660);
    cy.get('.extra-small-screen').should('not.be.visible');
    cy.get('.small-to-large-screen').should('not.be.visible');
    cy.get('.large-screen').should('be.visible');
  });

});
