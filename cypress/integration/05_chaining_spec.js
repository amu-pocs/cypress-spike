describe('Chaining', () => {

  it('should list full content', () => {
    cy.visit('http://localhost:4200/movies');

    cy.get('ul').children()
      .should('not', 'contain', 'We should never see this text');

    cy.get('ul').children()
      .should('contain', 'Incredibles 2');

      cy.get('ul').children().first()
        .should('contain', 'Mission: Impossible - Fallout');

      cy.get('ul').children().last()
        .should('contain', 'Talking to the Air: The Horses of the Last Forbidden Kingdom');
  });

});
