describe('URLs and Links', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4200');
  });

  it('should have a link to the movies page', () => {
    cy.get("a[href='/movies']")
      .click();
    cy.url().should('contain', 'movies');
    cy.url().should('not', 'contain', 'film');
  });

});
