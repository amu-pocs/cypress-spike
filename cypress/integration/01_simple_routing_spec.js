describe('Routing Tests', () => {
  it('should default to the home page', () => {
    cy.visit('http://localhost:4200')
    cy.contains('Home Page')
  });

  it('should render the home page', () => {
    cy.visit('http://localhost:4200/home')
    cy.contains('Home Page')
  });

  it('should render the about page', () => {
    cy.visit('http://localhost:4200/about')
    cy.contains('About Page')
  });

  it('should render the movie page', () => {
    cy.visit('http://localhost:4200/movies')
    cy.contains('Movies Page')
  });

});
