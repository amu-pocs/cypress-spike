describe('Input Elements', () => {

  it('should list full content', () => {
    cy.visit('http://localhost:4200/movies');
    cy.get('input').type('Mission');
    cy.get('.movie-title').contains('Mission');
  });

  it('should list full content', () => {
    cy.visit('http://localhost:4200/movies');
    cy.get('input').type('am');
    cy.get('.movie-title').contains('Cam');
  });

});
