const movies = [
  {
    'id': 1,
    // tslint:disable-next-line
    'overview': "When an IMF mission ends badly, the world is faced with dire consequences. As Ethan Hunt takes it upon himself to fulfill his original briefing, the CIA begin to question his loyalty and his motives. The IMF team find themselves in a race against time, hunted by assassins while trying to prevent a global catastrophe.",
    'poster_path': '/AkJQpZp9WoNdj7pLYSj1L0RcMMN.jpg',
    'release_date': '2018-07-13',
    'title': 'Mission: Impossible - Fallout',
    'vote_average': 7.3
  },
  {
    'id': 2,
    // tslint:disable-next-line
    'overview': "Vignettes weaving together the stories of six individuals in the old West at the end of the Civil War. Following the tales of a sharp-shooting songster, a wannabe bank robber, two weary traveling performers, a lone gold prospector, a woman traveling the West to an uncertain future, and a motley crew of strangers undertaking a carriage ride.",
    'poster_path': '/gBsf15wgAjMxX8l0lT711MovhSw.jpg',
    'release_date': '2018-11-09',
    'title': 'The Ballad of Buster Scruggs',
    'vote_average': 7.3
  },
  {
    'id': 3,
    // tslint:disable-next-line
    'overview': "Elastigirl springs into action to save the day, while Mr. Incredible faces his greatest challenge yet – taking care of the problems of his three children.",
    'poster_path': '/x1txcDXkcM65gl7w20PwYSxAYah.jpg',
    'release_date': '2018-06-14',
    'title': 'Incredibles 2',
    'vote_average': 7.6
  },
  {
    'id': 4,
    // tslint:disable-next-line
    'overview': 'Robert McCall, who serves an unflinching justice for the exploited and oppressed, embarks on a relentless, globe-trotting quest for vengeance when a long-time girl friend is murdered.',
    'poster_path': '/cQvc9N6JiMVKqol3wcYrGshsIdZ.jpg',
    'release_date': '2018-07-19',
    'title': 'The Equalizer 2',
    'vote_average': 6.3
  },
  {
    'id': 5,
    // tslint:disable-next-line
    'overview': "See the rise of the Guadalajara Cartel as an American DEA agent learns the danger of targeting narcos in 1980s Mexico.",
    'popularity': 54.713,
    'poster_path': '/aXwMx8OvRFMfI1RmkbJm6Bq1jVg.jpg',
    'title': 'Narcos: Mexico',
    'vote_average': 8.5
  },
  {
    'id': 6,
    // tslint:disable-next-line
    'overview': "A deep sea submersible pilot revisits his past fears in the Mariana Trench, and accidentally unleashes the seventy foot ancestor of the Great White Shark believed to be extinct.",
    'poster_path': '/eyWICPcxOuTcDDDbTMOZawoOn8d.jpg',
    'release_date': '2018-08-09',
    'title': 'The Meg',
    'vote_average': 6
  },
  {
    'id': 7,
    // tslint:disable-next-line
    'overview': "Gellert Grindelwald has escaped imprisonment and has begun gathering followers to his cause—elevating wizards above all non-magical beings. The only one capable of putting a stop to him is the wizard he once called his closest friend, Albus Dumbledore. However, Dumbledore will need to seek help from the wizard who had thwarted Grindelwald once before, his former student Newt Scamander, who agrees to help, unaware of the dangers that lie ahead. Lines are drawn as love and loyalty are tested, even among the truest friends and family, in an increasingly divided wizarding world.",
    'poster_path': '/uyJgTzAsp3Za2TaPiZt2yaKYRIR.jpg',
    'release_date': '2018-11-14',
    'title': 'Fantastic Beasts: The Crimes of Grindelwald',
    'vote_average': 7.2
  },
  {
    'id': 8,
    // tslint:disable-next-line
    'overview': "When the puppet cast of a '90s children's TV show begins to get murdered one by one, a disgraced LAPD detective-turned-private eye puppet takes on the case.",
    'poster_path': '/rWxkur51srfVnMn2QOFjE7mbq6h.jpg',
    'release_date': '2018-08-22',
    'title': 'The Happytime Murders',
    'vote_average': 5.7
  },
  {
    'id': 9,
    // tslint:disable-next-line
    'overview': "A young camgirl discovers that she’s inexplicably been replaced on her site with an exact replica of herself.",
    'poster_path': '/rdMkq7OrLSbG7t61BVNvmWuX1mJ.jpg',
    'release_date': '2018-10-01',
    'title': 'Cam',
    'vote_average': 5.9
  },
  {
    'id': 10,
    // tslint:disable-next-line
    'overview': "The world’s most magnificent horsemen face an unsure future in one of the planet’s last great equine cultures.  The Tibetan Buddhist region of Mustang in the High Himalaya is the Last Forbidden Kingdom and their unique heritage and remarkable spiritual bond with the horse is under threat. In a land where a man’s wealth can still be measured in horses, death defying races are the colorful back-drop for this story of the ascent of civilization in the high Himalaya. With lush cinematography, and insightful intervieww, the film also recounts the little known story of the CIA’s covert operations in Mustang, and features rare archival footage of the Dalai Lama’s flight on horseback over the Himalaya.  The scholarly and perceptive voices of Dr. Sienna Craig - author of 'Horses Like Lightning' and Mikel Dunham, author of 'Buddha's Warriors' turn this lens to issues of globalization, fragile border politics and the precarious future for Mustang’s distinctive equine culture.",
    'poster_path': '/1rsJGn52Ga0LkwKRnYXusein2HK.jpg',
    'release_date': '2016-08-01',
    'title': 'Talking to the Air: The Horses of the Last Forbidden Kingdom',
    'vote_average': 10
  },
];

export default movies;
