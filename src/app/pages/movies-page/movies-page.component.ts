import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable , of } from 'rxjs';
import * as FilmActions from '../../store/film/film.actions';

@Component({
  selector: 'app-movies-page',
  templateUrl: './movies-page.component.html',
  styleUrls: ['./movies-page.component.scss']
})
export class MoviesPageComponent implements OnInit {
  @Input() filterName;
  resultsFromStore$;
  fullResults;
  results$;

  constructor(private store: Store<any>) {

    this.resultsFromStore$ = this.store.select(state => {
      return state.film.results;
    }).subscribe((val) => {
      this.fullResults = val;
      this.results$ = val;
      return val;
    });
  }

  ngOnInit() {
  }

  onKey ($event) {
    this.results$ = this.fullResults.filter((val) => {
      return val['title'].match($event.target.value);
    });
  }

}
