import { FilmDetailsService } from './../../services/film-details.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-movie-page',
  templateUrl: './movie-page.component.html',
  styleUrls: ['./movie-page.component.scss']
})
export class MoviePageComponent implements OnInit {
  public film: Object = {};
  constructor(private filmDetailsService: FilmDetailsService, route: ActivatedRoute) {

    route.paramMap.subscribe(snapshot => {
      const id = snapshot['params']['id'] - 1; // minus one because array index start at 0
      filmDetailsService.getDetails(id).then((details) => {
        console.log('....', details);
        this.film = details;
      });
    });

  }

  ngOnInit() {
  }

}
