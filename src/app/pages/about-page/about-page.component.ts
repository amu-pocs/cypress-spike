import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-about-page',
  templateUrl: './about-page.component.html',
  styleUrls: ['./about-page.component.scss']
})
export class AboutPageComponent implements OnInit {

  results$;

  constructor(public http: HttpClient) { }

  ngOnInit() { }

  loadCatDetails () {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*'
      })
    };
    // this url won't work
    this.results$ = this.http.get(`http://api.tvmaze.com/shows?page=1`);
  }



}
