import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { FilmActionTypes } from './film.actions';

@Injectable()
export class FilmEffects {

  @Effect()
  loadFoos$ = this.actions$.pipe(ofType(FilmActionTypes.LoadFilms));

  constructor(private actions$: Actions) {}
}
