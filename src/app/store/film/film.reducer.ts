import { Action } from '@ngrx/store';
import { FilmActions, FilmActionTypes } from './film.actions';
import movieList from '../../movies-list';

export interface State {

}

export const initialState: State = {
  results: movieList
};
export function reducer(state = initialState, action: FilmActions): State {
  switch (action.type) {

    case FilmActionTypes.LoadFilms:
      return state;


    default:
      return state;
  }
}
