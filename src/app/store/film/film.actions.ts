import { Action } from '@ngrx/store';

export enum FilmActionTypes {
  LoadFilms = '[Film] Load Films'
}

export class LoadFilms implements Action {
  readonly type = FilmActionTypes.LoadFilms;
}

export type FilmActions = LoadFilms;
