import { Injectable } from '@angular/core';
import movieList from '../movies-list';

@Injectable({
  providedIn: 'root'
})
export class FilmDetailsService {

  constructor() { }

  getDetails (id) {
    const promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(movieList[id]);
      }, 1000);
    });
    return promise;

  }
}
