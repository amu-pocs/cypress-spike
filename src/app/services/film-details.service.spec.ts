import { TestBed, inject } from '@angular/core/testing';

import { FilmDetailsService } from './film-details.service';

describe('FilmDetailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FilmDetailsService]
    });
  });

  it('should be created', inject([FilmDetailsService], (service: FilmDetailsService) => {
    expect(service).toBeTruthy();
  }));
});
