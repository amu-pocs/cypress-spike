# cypress-spike

### Create repo
then install angular

```
ng new cypress-spike --routing --style=scss
```

### Add NgRx

```
npm install @ngrx/schematics --save-dev

npm install @ngrx/{store,effects,entity,store-devtools} --save

ng config cli.defaultCollection @ngrx/schematics

```

The ngRx schematics (currently 26.07.2018) do not properly support the scss or less style extensions, so put this into the angular.json to continue using scss:

```
"schematics": {
  "@ngrx/schematics:component": {
    "styleext": "scss"
  }
}
```

### Create store

```

# Create a store for the app. This store will aggrigate the features 
ng generate store State --root --module app.module.ts

# Generate Effects...
ng generate effect App --root --module app.module.ts


```

### Generate a store

```
ng generate feature store/film/film --flat
```

Import
 
```

import * as fromFilm from './store/film/film.reducer';
import { FilmEffects } from './store/film/film.effects';

// and...

imports: [
  ...
  StoreModule.forFeature('film', fromFilm.reducer),
  EffectsModule.forRoot([AuthEffects])
  ...
]
```

### Add Styles

```
# vim ~/.npmrc 
registry=https://nexus.cd.auspost.com.au/nexus/content/groups/auspost-ddc-npmjs/
strict-ssl=false
```

```
npm install mpc-ap-styles --save

```

Add the fonts to assets in angular.json

```
{
  "glob": "**/*",
  "input": "node_modules/auspost-styles/dist/fonts",
  "output": "/assets/fonts"
}
```


### Generate Some pages

```
ng g component pages/home-page
ng g component pages/about-page
ng g component pages/movies-page
```



